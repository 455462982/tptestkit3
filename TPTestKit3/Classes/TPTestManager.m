//
//  TPTestManager.m
//  TPTestKit
//
//  Created by tengpan on 2017/11/30.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import "TPTestManager.h"

@implementation TPTestManager

+ (NSString *)version
{
    return @"1.0.0";
}

@end
